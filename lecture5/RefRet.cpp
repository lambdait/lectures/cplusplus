#include <iostream>

double val = 100.0;
double &f()
{
    return val; // Возвращаем ссылку на val;
}


int main()
{
    double newval;

    std::cout << f() <<'\n';

    newval = f();
    
    std::cout<< newval <<'\n';
    f() = 99.1;
    std::cout<<f()<<'\n';

    return 0;
}