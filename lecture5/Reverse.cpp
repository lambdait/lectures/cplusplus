#include <iostream>

void reverse(char *s)
{
    if(*s)
        reverse(s+1);
    else
        return;
    std::cout<<*s;
}

int main()
{
    char str[] ="This is text";
    reverse(str);
    return 0;
}