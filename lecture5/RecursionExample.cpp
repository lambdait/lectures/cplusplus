#include <iostream>

//Рекурсивная версия
int factr(int n)
{
    int answer;
    if (n==1) return 1;
    answer = factr(n-1)*n;
    //std::cout<<answer<<"\n";
    return answer;
}
//Итеративная версия
int fact(int n)
{
    int answer=1;
    for(int t=1; t<=n;t++) answer = answer*t;
    return answer;
}

int main()
{
    std::cout<<"factr(4)="<<factr(4)<<"\n";
    std::cout<<"fact(4)="<<fact(4)<<"\n";
    return 0;
}
