# Лекция 5. Функции. Продолжение

## Прототипы функций

Прототип объявляет функцию до первого использования. Необходимое условия при написании кода на С++ - информация о возращаемом типе,типе параметров и количестве параметров должна быть объявлена до начала основной программы.

Эта необходимость вызвана тем, что компилятору необходимо заранее знать все вышеуказанные параметры для проверки на соответсвие при вызове функции.

## Рекурсия

Рекурсивная функция - это функция, которая вызываетс сама себя. По сути при своем опрделении она использует саму себя же. Классичесий пример: вычисление факториала.

```cpp
#include <iostream>

//Рекурсивная версия
int factr(int n)
{
    int answer;
    if (n==1) return 1;
    answer = factr(n-1)*n;
    return answer;
}
//Итеративная версия
int fact(int n)
{
    int answer=1;
    for(int t=1; t<n;t++) answer = answer*t;
    return answer;
}

int main()
{
    std::cout<<"Факториал числа 4 равен"<<factr(4);
    std::cout<<"Факториал числа 4 равен"<<fact(4);
    return 0
}

```

Когда функция вызывает сама себя,  в системном стеке выделяется память для новых локальных переменных и параметров, и когд функции выполняется с этими новыми переменными. Рекуривный вызов не создает копии функции. Новыми являются только аргументы. При возвращении каждого рекрсивного вызова из стека извлекаются старые локальные переменные и параметры, и выполнение функции возоновляется с "внутренней" точки ее вызова. 

Преимущества использования рекурсии:

* позволяют упростить реализацию некоторых алгоритмов

* наиболее адекватный способ реализации переменного числа вложенных циклов

Недостатки использования рекурсии:

* из-за использования стека выполнения рекурсивных программ происходит медленнее, чем у их итерационных собратьев

* "съедают" много памяти

При написании рекурсивной функции необходимо включать в нее инструкцию проверки условия (например, if-инструкцию), которая бы обеспечивла выход из функции без выполнения рекурсивного вызова. Если это не сделать, то, вызвав однажды такую функцию, из нее уже нельзя будет вернуться.

# Ссылки, перегрузка и использование аргументов по умолчанию

Как мы уже знаем, сущетсву два способа передачи аргументов:

* передается значение аргумента - вызов по значению (call-by-value)

* передается адрес аргумента - вызов по ссылке (call-by-reference)

По умолчанию в С++ при вызове функции в нее передается значение аргумента `function(10)`, где 10 - не что иное, как целочисленный параметр.

**UPD**: Важно помнить, что в функцию передается копия аргумента, то что происходит внутри функции никак не отражаетя на значении переменной, используемой при вызове функции.

Из замечания выше, очевино, что вызов по ссылке (передача адреса) по смыслу полностью противоположен: мы "дистанционно" меняем значение переменной.

Пример:

```cpp
void swap(int* x, int* y)
{
    int temp;

    temp = *x; // Временно сохраняем значения, расположенное по адресу x

    *x = *y; // Помещаем значение, хранимое по адресу y, по адресу x.

    *y = temp;  // Помещаем значение, которое раньше хранилось по адресу x, по адресу y.
}
```

## Ссылочные праметры

Описанные выше операции с указателями зачастую являются неудобными, т.к. необходимо все врем помнить, где передается аргумент, а где указатель. Во избежание всех неудобств, связанных с указателями, при необходимости "дистанционного" изменения переменных, были введены ссылочные параметры. 

Ссылочный параметр автоматически получает адрес соответствуюшщего аргмента.

Пример:

```cpp
#include <iostream>

void f(int &i) // Символ & превращает переменную в ссылочный парметр
{
    i = 10;
}

int main()
{
    int val =1;
    std::cout<<val<<'\n';
    f(val);
    std::cout<<val<<'\n';
}
```

 Мы видим, что `i = 10` не присваивает переменной i значение 10, а 10 присваивается переменной, на которуюссылает переменная i (в нашем случае на val). Обратите внимание: при вызове функции `f()` мы не используем оператор `&`, более того это было бы ошибкой. Мы передаем туда аргумент, компилятор же сам понимает, что ему необходимо передать адрес переменной.

 Аналогичным образом мы можем переписать нашу функцию `swap` без использования указателей:

 ```cpp
 void swap(int &x, int&y)
 {
     int temp;

     temp = x; // Сохраняем значение, расположенное по адресу x.

     x = y; // Помещаем значение, хранимое по адресу y, по адресу x;

     y = tempж // Помещаем значение, которое раньше хранилось по адресу x, по адресу y;
 }
 ```

 **UPD**: Находясь внутри функции и присваивая некоторое значение ссылке, вы в действительности присваиваете это значение переменной, на которую указывает эта ссылка, т.е. применяя ссылку в качестве аргумента вы действительно взаимодействуете с переменной.

 **UPD2**: В языке C ссылки не поддерживаются! Возможен только метод с использованием указателей.

## Возврат ссылок

Если функция возвращает ссылку, это значени, что на она возвращает неявный указатель на значение, передаваемое ею в инструкцию `return`. Отсюда вытекает появление очень полезного трюка: функцию можно использовать в левой части присваивания.

Пример:

```cpp
#include <iostream>

double val = 100.0;
double &f()
{
    return val; // Возвращаем ссылку на val;
}


int main()
{
    double newval;

    std::cout << f() <<'\n'; // Отображаем значение val

    newval = f(); 
    
    std::cout<< newval <<'\n';
    f() = 99.1; // Изменяем значение val
    std::cout<<f()<<'\n';

    return 0;
}

```

Пожалуй самой интересной строкой в данном фрагменте является `f() = 99.1`. Поскольку функция f() возвращает ссылку на перемнную val, эта ссылка и является премником инструкции присваивания. Таким образом, значениние `99.1`, присваиваетя переменной val косвенно, через ссылку на нее, которую возвращает функция f().

Рассмотрим еще один пример:

```cpp
#include <iostream>

double &change_it(int i);

double vals[] = {1.1, 2.2, 3.3, 4.4, 5.5};

int main()
{
    std::cout<<"Input values: ";
    for(int i=0; i<5; i++)
        std::cout<<vals[i]<<' ';
    std::cout<<'\n';

    change_it(1) = 5298.23; // Изменяем 2-й элемент
    change_it(3) = -98.8; // Изменяем 4-й элемент

    std::cout<< "Changed values: ";
    for(int i=0; i<5; i++)
        std::cout<<vals[i]<<' ';
    std::cout<<'\n';
    return 0;
}

double &change_it(int i)
{
    return vals[i]; // Возвращаем ссылку на i-й элемент
}

```

## Независимая ссылка

Независимая ссылка - это просто еще одно название для переменных некоторого иного типа. Она должна указывать на некоторый объект, и, следовательно, инициализирована при объявлении.

Чтобы понять, что такое независимая ссылка, рассмотрим пример:

```cpp
#inlude <iostream>

int main()
{
    int j,k;
    int &i = j; // Независимая ссылка

    j = 10;
    std::cout<<j<<' '<<i; // Выводится: 10 10
    k = 121;
    i = k; // Копирует в переменную j значение переменной k, а не адрес переменной k
    std::cout<<'\n'<<j;

    return 0;
}
```

Как вы уже поняли в данном примере две переменные параллельно хранят и меняют одно и то же значение. Этот трюк применяется довольно редко, а их использование может сбить с толку, как правило можно найти альтернативную замну данному трюку.

## Огрничения при использовании ссылок

На применение ссылочных переменных накладывается ряд ограничений:

* Нельзя ссылаться на ссылочную переменную.

* Нельзя создавать массивы ссылок.

* Нельзя создавать указаттель на ссылку, т.е. нельзя к ссылке применить оператор "&".

# Перегрузка функций

Перегрузка функций (function overloading) - это механизм, который позволяет двум родственным функциям иметь одинаковые имена, но при условии, что их параметры (тип параметров, их количество) будут различны.

Рассмотрим пример:

```cpp
#include <iostream>

void f(int i);
void f(int i, int j);
void f(double k);

int main()
{
    f(10);

    f(10,20);

    f(12.23);

    return 0;
}

void f(int i)
{
    std::cout<<"In function f(int), i = "<<i<<'\n';
}

void f(int i, int j)
{
    std::cout<<"In function f(int,int), i = " << i << ", j = " << j <<'\n';
}

void f(double k)
{
    std::cout<<"In function f(double), k = " << k << '\n';
}
```

**UPD**: Для определения того, какую функцию из перегруженных нужно вызывать компилятору, ему необхоимо различать количество и типы аргументов, при этом, несмотря на то, что функции с одинаковми аргументами могу возвращать разные значения, компилятор не сможе определить, какой тип ему нужно вернуть.

## Аргументы, передаваемые функции по умолчанию

В С++ мы можем придать параметру некоторое значение, которое будет автоматически использвано, если при вызове функции не задается аргумент, соответствующий этому параметру. Аргументы, передаваемые функции по умолчанию, можно использовать, чтобы упростить, обращение к сложным функциям, а так же в качестве "сокращенной формы" перегрузки функций.

Например:

```cpp
void myfunc(double num = 0.0, char ch = 'X') { ... }
```

**UPD** Важно помнить, что параметры, передаваемые по умолчанию, должны быть справа от обычных параметров.

## Перегрузка функций и неоднозначность

Неоднозначность возникает тогда, когда компилятор не может определить различие между двумя перегруженными функциями.

Основной причиной неоднозначности в С++ является автоматическое преобразование типов, например:

```cpp
int myfunc(double d);
.
.
.
std::cout<< myfunc('c'); // Ошибки нет, выполняется преоразование типов
```

Вообще говоря, в С++ запрещено довольно мало преобразований типов. Сама неочевидность может быть выражена следующим образом:

```cpp
float myfunc(float i);
double myfunc(double i);
...
std::cout<<myfunc(10.1); // Неоднознаности нет, вызывается myfunc(double)

std::cout<<myfunc(10); // Неоднозначность
```
