#include <iostream>

void f(int i);
void f(int i, int j);
void f(double k);

int main()
{
    f(10);

    f(10,20);

    f(12.23);

    return 0;
}

void f(int i)
{
    std::cout<<"In function f(int), i = "<<i<<'\n';
}

void f(int i, int j)
{
    std::cout<<"In function f(int,int), i = " << i << ", j = " << j <<'\n';
}

void f(double k)
{
    std::cout<<"In function f(double), k = " << k << '\n';
}