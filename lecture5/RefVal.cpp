#include <iostream>

double &change_it(int i);

double vals[] = {1.1, 2.2, 3.3, 4.4, 5.5};

int main()
{
    std::cout<<"Input values: ";
    for(int i=0; i<5; i++)
        std::cout<<vals[i]<<' ';
    std::cout<<'\n';
    
    change_it(1) = 5298.23; // Изменяем 2-й элемент
    change_it(3) = -98.8; // Изменяем 4-й элемент

    std::cout<< "Changed values: ";
    for(int i=0; i<5; i++)
        std::cout<<vals[i]<<' ';
    std::cout<<'\n';
    return 0;
}

double &change_it(int i)
{
    return vals[i]; // Возвращаем ссылку на i-й элемент
}