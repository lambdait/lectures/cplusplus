#include <iostream>
#include <cstdlib>

void drill();

int count;
int num_right; // Глобальные переменные

int main()
{
    std::cout<<"Сколько практических упражнений: ";
    std::cin>>count;

    num_right = 0;
    do {
        drill();
        count--;
    } while(count);
    std::cout<<"Вы дали "<< num_right << " правильных ответов";
    return 0;
}

void drill()
{
    int count; // Эта переменная count - локальная и никак не связана с одноименной глобальной

    int a,b, ans;
    // Генерируем два числа между 0 и 99
    a = std::rand() % 100;
    b = std::rand() % 100;

    // Пользователь получает три попытки дать правильный ответ
    for(count=0; count<3; count++)
    {
        std::cout<<"Сколько будет "<< a << "+" << b <<"?";
        std::cin>> ans;
        if(ans == a+b)
        {
            std::cout<<"Правильно\n";
            num_right++;
            return;
        }
    }
    std::cout<< "Вы использовали все свои попытки.\n";
    std::cout<< "Ответ равен " << a+b << '\n';
}
