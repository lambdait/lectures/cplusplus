#include <iostream>

const char *find_substr(const char *sub,const char *str);

int main()
{
    const char *substr;
    substr = find_substr("три", "один два три четыре");
    std::cout<< "Найденная подстрока: "<<substr;
    return 0;
}

// Функция возвращает указатель на искомую подстроку или 0, если не найдет такую
const char *find_substr(const char *sub,const char *str)
{
    int t;
    const char *p, *p2, *start;
    for(t=0; str[t]; t++)
    {
        p = &str[t];
        start = p;
        p2 = sub;
        while(*p2 && *p2==*p)
        {
            p++;
            p2++;
        }
        // Если достигнут конец p2-подстроки, то эта строка была найдена
        if(!*p2)
            return start;
    }
    return 0; // Подстрока не найдена
}
