#include <iostream>

int main(int argc, char *argv[])
{
    if(argc!=2)
    {
        std::cout<<"Вы забыли ввести своё имя.\n";
        return 1;
    }
    std::cout<<"Привет, "<<argv[1]<<'\n';
    return 0;
}
