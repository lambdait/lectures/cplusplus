#include <iostream>
#include <cstring>

int find_substr(const char *sub, const char *str);
int my_find_substr(const char *sub, const char *str);

int main()
{
    int index, s_index;
    index = find_substr("три", "один два три четыре");
    s_index = my_find_substr("три", "один два три четыре");
    std::cout<< "Индекс равен "<<index<<'\n'; // Индекс равен 9
    std::cout<< "Индекс равен "<<s_index<<'\n';
    return 0;
}

// Функция возвращает индекс искомой подстроки или -1, если она не была найдена
int find_substr(const char *sub,const char *str)
{
    int t;
    const char *p, *p2;
    for(t=0; str[t]; t++)
    {
        p = &str[t];
        p2 = sub;
        while(*p2 && *p2 ==*p) //проверка совпадения
        {
            p++;
            p2++;
        }
        if(!*p2) return t;
    }
    // Если был достигнут конец p2-строкки (подстроки), то подстрока была найдена
    return -1;
}

int my_find_substr(const char *sub, const char *str)
{
    int sub_size = std::strlen(sub);
    int str_size = std::strlen(str);
    const char *end = str + str_size - sub_size;

    for (const char *cur = str; cur <= end; cur++)
        if (std::strncmp(cur, sub, sub_size) == 0)
            return cur - str;

    return -1;
}

