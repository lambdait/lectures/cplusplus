#include <iostream>
#include <cctype>
#include <cstring>

void stringupper(char *str);

int main()
{
    char str[80];
    std::strcpy(str, "I like C++, but I'm not sure");
    stringupper(str);
    std::cout<<str;
    return 0;
}

void stringupper(char *str)
{
    while(*str)
    {
        *str = std::toupper((unsigned char) *str);
        str++;
    }
}
