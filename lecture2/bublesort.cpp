#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <ctime>

int main()
{
    int nums[10];
    int size = 10;
    for(int t = 0; t<size; t++)
        nums[t] = std::rand();

    std::cout<< "Исходный массив: \n";
    for(int t = 0; t<size; t++)
        std::cout<<nums[t]<<"\n";
    //Сортировка пузырьком
    for(int a=1; a<size; a++)
        for(int b=size-1; b>=a; b--)
            if(nums[b-1] > nums[b])
                std::swap(nums[b-1],nums[b]);

    std::cout<<"\n";
    std::cout<< "Отсортированный массив: \n";
    for(int t = 0; t<size; t++)
        std::cout<<nums[t]<<"\n";
    return 0;
}
