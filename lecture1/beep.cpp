#include <iostream>
#include <thread>
#include  <chrono>

int main()
{
    std::cout<<"Beep at the end of the messeage\a\n";
    for(int i=0; i<10; i++)
    {
        std::this_thread::sleep_for(std::chrono::seconds(1));
        std::cout<<"Beep again\a\n";
    }
    return 0;
}
