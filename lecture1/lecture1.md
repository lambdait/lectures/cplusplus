# Лекция №1
![](https://pp.userapi.com/c852136/v852136888/1994e/cWeV8U-breE.jpg)

## Для чего нужен C++?

C++ является одним из самых популярных языков программирования, как ни крути.
Казалось бы с появлением C#, Java и супеперпопулярного и простого языка Python,
у C++ нет дальнейших перспектив. Но история на стороне С++. 
Мы редко задумывались о том, на чем написан наш софт, компиляторы и оказывается
это было большей частью реализовано на C++. В своих проектах его используют
множество различных компаний, среди которых Addobe, 
Microsoft, IBM, Mozila, Google и т.д.  Компилятор для Python и JVM
(Java Virtual Machine) были написаны на С++.  
Вот [здесь](http://www.stroustrup.com/applications.html) полный перечень всего того, 
кем и как используется С++.

![Рейтинг языков согласно TIOBE](https://i.imgur.com/ZCfBqoa.png)

### Немного истории

Оффициальным создателем языка считается Бьерн Страуструп, язык увидел свет в 
1980 году. Остальное вы можете почитать на [Википедии](https://ru.wikipedia.org/wiki/C%2B%2B). 

### Компилятор? Строго типизированный язык?

В опрделении из вики говорится, что это язык “компилируемый, статически
типизированный язык программирования общего назначения”.
Начнем с конца: статистически типизированный – значит , что в нем строго
определены типы данных (integer, float, char и т.д.), 
под каждый из которых выделяется определенное количество памяти. 
Подробнее о типах данных мы поговорим на следующей лекции.

Если коротко, компилятор – специальная программа, выполняющая сборку всех 
включенных в исходную программу  паетов и модулей и перевод собранных данных в машинный код.
Подробнее можно посмотреть [здесь](https://habr.com/sandbox/114988/)

## Что нам понадобится

В первую очередь для разработки на языке С понадобятся следующие инструменты:

- Текстовый редактор;
- Компилятор;
- Отладчик;
- Система контроля версий.

Некоторые из инструментов, о которых речь идет ниже, являются проектами с
открытым исходным кодом (open source), другие распространяются бесплатно в составе 
различных программ для студентов или имеют бесплатный план использования. 
Так или иначе я вам рекомендую получить студенческие пакеты от Github и JetBrains:

- [Github Student Developer Pack](https://education.github.com/pack)
- [JetBrains For Students](https://www.jetbrains.com/student/)

### Текстовый редактор

Существует огромное количество различных текстовых редакторов, среди которых выделяются
[Emacs](https://www.gnu.org/software/emacs/) и [Vim](https://www.vim.org/). 
Это **крайне** мощные инструменты, обладающие большим потенциалом для расширения.
Так или иначе, на освоение в них вам потребуется некоторое время 
([смотри вот эту ветку](https://stackoverflow.com/questions/11828270/how-to-exit-the-vim-editor)).
Среди редакторов попроще зачастую выбирают [Sublime](https://www.sublimetext.com/),
[Atom](https://atom.io/) или [Visual Studio](https://code.visualstudio.com/). 
Несмотря на достаточно низкий уровень вхождения, эта группа редакторов также позволяет 
использовать (и писать свои) расширения, настраивать работу и внешний вид и т.д.

### Компилятор

Пока что будем рассматривать компилятор как некотурую сущность, котороя переводит наши программы с языка C в машинный код (на самом деле процесс сложнее, но пока детали опустим). В ходе данного курса в качестве компилятора будет использоваться [GCC](https://gcc.gnu.org/). Если вы используете Linux или macOS, то, скорее всего, пакет gcc у вас уже установлен (чтобы убедиться в этом файле введите в вашей командной оболочке `gcc --version`). Если вы используете ОС семейства Windows, то есть как минимум 3 способа для его установки:

- [MinGW](http://mingw.org/)
- [Cygwin](https://cygwin.com/)
- Windows Subststem for Linux

#### MinGW

MinGW, сокращение от "Minimalist GNU for Windows", предоставляет вам порты программ
проекта GNU/Linux для использования их в командной оболочке Windows. 
Более подробно процесс установки MinGW описан [здесь](http://mingw.org/wiki/Getting_Started).

#### Cygwin

Cygwin предоставляет достаточно большую коллекциб программ GNU, а также содержит 
в себе эмулятор командной оболочки bash. Более подробно процесс установки Cygwin 
описан [здесь](https://cygwin.com/cygwin-ug-net/ov-ex-win.html).

#### Windows Subststem for Linux

Windows Subststem for Linux - специальный инструмент Windows 10, который позволяет 
быстро и просто получить доступ к эмулятору bash на Windows.
Более подробно процесс установки Windows Subststem for Linux описан [здесь](https://docs.microsoft.com/en-us/windows/wsl/install-win10).

#### Другие варианты

Если ничего из перечисленного выше не близко вашей тонкой душе, то можно проявить фантазию:

- Установить один из множества дистрибутивов Linux в качестве Dual Boot'a на свой компьютер;
- поставить виртуальную машину на свою систему с Linux;
- найти еще один компьютер и поставить на него Linux;
- купить VPS и доступаться к нему по SSH чтобы компилировать свои программы.

И так далее.

#### Компиляция программ на С++

Для компиляции будем использовать следующую команду:

```
g++ <исходный файл>.cpp -Wall -Wextra -pedantic-errors -std=c++2a -o <исполнимый файл>.exe
```

* g++ - программа-компилятор 
* Wall - включает основные предупреждения
* Wextra - включает дополнительные предупреждения
* pedantic-errors - включает ошибки, если код не соответствует стандарту
* std=c++2a - выбирает версию языка. Существуют c++98, c++11, c++14, c++17 и самая новая c++2a


### IDE

Альтернативной заменой (и наиболее удобной) текстовому редактору можно назвать IDE (Integrated Development Environment) - 
интегрированная среда разработки, которая позволяет упростить взаимодейстиве с кодом, 
а именно посредством наглядной визуализация процесса отладки программы и при упрощеннного интерфейса для компиляции и сборки.

Среди наоболее популярных стоит отметить:
* Microsft Visual Studio
* CLion
* Eclipse
* CodeBlocks

### Система контроля версий

Система контроля версий необходима не только для обеспечения процесса совместной работы нескольких людей над одним проектом. Она также позволяет индивидуальному разработчику эффективно управлять процессом написания исходного кода, разрабатывать новые features, при этом имея возможность откатиться и т.д. В рамках данного курса мы будем использовать [Git](https://git-scm.com/) для контроля версий.

###### Краткая справа по Git

|Команда    |Значение    |
| -         |:----------:|
|git init   |инициализирует git-репозиторий в текущей директории|
|git add    |<путь к файлу или директории> добавляет файлы в staging|
|git commit -m "ваше сообщение"|создает коммит с вашем сообщением|
|git branch <название ветки>|создает новую ветку|
|git checkout <название ветки>|переключается на ветку|
|git checkout --.|отменает все изменения, которые не были добавлены в коммит|
|git checkout <commit hash>|откатиться к коммиту с указаным хешем|

Более подробно Git рассмотрен в других лекциях сообщества Lambda:

- [Лекция 1](https://github.com/lambdafrela/lambda-help/tree/master/lectures/git/lecture_1)
- [Лекция 2](https://github.com/lambdafrela/lambda-help/tree/master/lectures/git/lecture_2)


## Первая программа на C++

Изучение любого языка по традиции начинается с программы "Hello, World!". Она позволяет попробовать на практике базовый синтаксис языка и узнать о функциях вывода.

Для вывода будем использовать стандартную функцию *cout*, описание которой содержится в прострастве имен std. При помощи пространства имен
мы указываем явным образом, что используется cout из стандартной библиотеки, а не какой либо другой *cout*.


```C++
/* комментарии в С++ пишутся вот так */

#include <iostream> /* директива #include говорит препроцессору, что необходимо включить текст файла iostream наш файл */

int main() /* точка вхождения в программу */
{
  std::cout<<"Hello, world!"; /* Строку заключаем в двойные кавычки */
}
```

Символ *\n* — т.н. управляющая последовательности (escape-sequence — последовательность нескольких символов, обозначающих специальную инструкцию устройства ввода/вывода), которая говорит, что после ее считывания продолжить печать нужно с новой строки. Вот список всех ескейп последовательностей:

|Последовательность| Значение|
| - |:-------------:|
|\a | подача звукового сигнала|
|\b | возврат назад и затирание|
|\f | прогон страницы|
|\n | конец строки|
|\r | возврат каретки|
|\t | горизонтальная табуляция|
|\v | вертикальная табуляция|
|\o00| восьмеричное число|
|\x00| шестнадцатеричное число|
|\\\\ | обратная косая черта|
|\\? | вопросительный знак|
|\\' | одинарная ковычка|
|\\" | двойная кавычка|

**Замечание:** последние четыре последовательности это так называемые *экранированные* символы. Т.е. мы ставим обратный слеш дважды, чтобы показать, что намерены использовать именно этот символ, а не какую-то специальную последовательность, начинающуюся с него, как бы *экранируя* его.

В конце каждой инструкции *языка* C++ ставится ";". Обратите внимание, что после директивы препроцессора ";" не стоит.
После компиляции и запуска программы мы получаем следующее:

Hello, World!

Awesome!

### Домашнее задание

1. Создать аккаунт на [Github](https://github.com/).
2. Активировать [Github Student Developer Pack](https://education.github.com/pack).
3. Активировать [JetBrains For Students](https://www.jetbrains.com/student/).
4. [Присоединиться](https://vk.com/away.php?to=https%3A%2F%2Flambdainvite.herokuapp.com%2F&cc_key=) к нашему [чату в Slack](https://lambdafrela.slack.com/).
5. Написать программу, которая выводит фразу "Hello, world!".
6. Создать свой git-репозиторий.
7. Загрузить git-репозиторий с программой на Github.
8. Почитать про GCC [здесь](http://www.network-theory.co.uk/docs/gccintro/).






